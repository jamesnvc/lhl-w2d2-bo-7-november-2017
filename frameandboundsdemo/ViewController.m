//
//  ViewController.m
//  frameandboundsdemo
//
//  Created by James Cash on 07-11-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UIView *pinkView;

@property (strong, nonatomic) IBOutlet UIView *chartreuseView;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    CGRect wholeFrame = self.view.frame;
    CGRect pinkFrame = self.pinkView.frame;
}

- (void)viewDidAppear:(BOOL)animated
{

    // This doesn't work
//    self.chartreuseView.frame.origin.y += 10;
// This is a little too verbose
    /*
     CGRect newFrame = self.chartreuseView.frame;
     CGPoint newOrigin = newFrame.origin;
     newOrigin.y += 10;
     newFrame.origin = newOrigin;
     self.chartreuseView.frame = newFrame;
     */
// This is the preferred approach
//    self.chartreuseView.frame = CGRectOffset(self.chartreuseView.frame, 0, 10);

    self.pinkView.bounds = CGRectOffset(self.pinkView.bounds, 0, 10);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
